package interfaces;

import java.util.List;


import javax.ejb.Remote;
import model.Chauffeur;

@Remote
public interface ChauffeurRemote {

	public void addChauffeur(Chauffeur c);
	public List<Chauffeur> getAllChaffeurs();
	public void deleteChauffeurById(int chauffeurId);
	public void updateChauffeur(Chauffeur c);
}
