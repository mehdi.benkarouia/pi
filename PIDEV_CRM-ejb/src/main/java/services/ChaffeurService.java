package services;

import java.util.List;


import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import interfaces.ChauffeurRemote;
import model.Chauffeur;


@Stateless
@LocalBean
public class ChaffeurService implements ChauffeurRemote{

	@PersistenceContext(unitName="primary")
	EntityManager em;
	
	@Override
	public void addChauffeur(Chauffeur c) {
		// TODO Auto-generated method stub
			em.persist(c);
		}

	@Override
	public List<Chauffeur> getAllChaffeurs() {
		List<Chauffeur> chauffeurs = em.createQuery("Select c from Chauffeur c",
				Chauffeur.class).getResultList();
		return chauffeurs;
	}

	@Override
	public void deleteChauffeurById(int chauffeurId) {
		// TODO Auto-generated method stub
		  Chauffeur chauffeur = em.find(Chauffeur.class, chauffeurId);
		  em.remove(chauffeur);
		}

	@Override
	public void updateChauffeur(Chauffeur c) {
		// TODO Auto-generated method stub
		em.merge(c);
	}
	
}



