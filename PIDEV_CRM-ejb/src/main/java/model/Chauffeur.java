package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Chauffeur database table.
 * 
 */
@Entity
@NamedQuery(name="Chauffeur.findAll", query="SELECT c FROM Chauffeur c")
public class Chauffeur implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;

	private String adresse;

	private String image;

	private String nom;

	private String prenom;

	private int tel;

	private String ville;

	public Chauffeur() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAdresse() {
		return this.adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getTel() {
		return this.tel;
	}

	public void setTel(int tel) {
		this.tel = tel;
	}

	public String getVille() {
		return this.ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public Chauffeur(String adresse, String image, String nom, String prenom, int tel, String ville) {
		super();
		this.adresse = adresse;
		this.image = image;
		this.nom = nom;
		this.prenom = prenom;
		this.tel = tel;
		this.ville = ville;
	}

	public Chauffeur(int id, String adresse, String nom, String prenom, int tel, String ville) {
		super();
		this.id = id;
		this.adresse = adresse;
		this.nom = nom;
		this.prenom = prenom;
		this.tel = tel;
		this.ville = ville;
	}


	

}