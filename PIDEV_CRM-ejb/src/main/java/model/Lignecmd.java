package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Lignecmds database table.
 * 
 */
@Entity
@Table(name="Lignecmds")
@NamedQuery(name="Lignecmd.findAll", query="SELECT l FROM Lignecmd l")
public class Lignecmd implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int lignecmdId;

	private int etat;

	private double prix;

	private int quantite;

	//bi-directional many-to-one association to ClientPhysique
	@ManyToOne
	@JoinColumn(name="UserId")
	private ClientPhysique clientPhysique;

	//bi-directional many-to-one association to FactDev
	@ManyToOne
	@JoinColumn(name="FactdevId")
	private FactDev factDev;

	//bi-directional many-to-one association to Produit
	@ManyToOne
	@JoinColumn(name="ProductId")
	private Produit produit;

	public Lignecmd() {
	}

	public int getLignecmdId() {
		return this.lignecmdId;
	}

	public void setLignecmdId(int lignecmdId) {
		this.lignecmdId = lignecmdId;
	}

	public int getEtat() {
		return this.etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public double getPrix() {
		return this.prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public int getQuantite() {
		return this.quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public ClientPhysique getClientPhysique() {
		return this.clientPhysique;
	}

	public void setClientPhysique(ClientPhysique clientPhysique) {
		this.clientPhysique = clientPhysique;
	}

	public FactDev getFactDev() {
		return this.factDev;
	}

	public void setFactDev(FactDev factDev) {
		this.factDev = factDev;
	}

	public Produit getProduit() {
		return this.produit;
	}

	public void setProduit(Produit produit) {
		this.produit = produit;
	}

}