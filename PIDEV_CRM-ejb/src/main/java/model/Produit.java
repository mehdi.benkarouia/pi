package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Produit database table.
 * 
 */
@Entity
@NamedQuery(name="Produit.findAll", query="SELECT p FROM Produit p")
public class Produit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String description;

	private String nom;

	private double prix;

	private int quantite;

	//bi-directional many-to-one association to Lignecmd
	@OneToMany(mappedBy="produit")
	private List<Lignecmd> lignecmds;

	//bi-directional many-to-many association to Pack
	@ManyToMany
	@JoinTable(
		name="ProduitPacks"
		, joinColumns={
			@JoinColumn(name="ProduitId")
			}
		, inverseJoinColumns={
			@JoinColumn(name="PackId")
			}
		)
	private List<Pack> packs;

	//bi-directional many-to-one association to Stock
	@ManyToOne
	@JoinColumn(name="Stock_id")
	private Stock stock;

	//bi-directional many-to-one association to ProduitOffre
	@OneToMany(mappedBy="produit")
	private List<ProduitOffre> produitOffres;

	public Produit() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public double getPrix() {
		return this.prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public int getQuantite() {
		return this.quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public List<Lignecmd> getLignecmds() {
		return this.lignecmds;
	}

	public void setLignecmds(List<Lignecmd> lignecmds) {
		this.lignecmds = lignecmds;
	}

	public Lignecmd addLignecmd(Lignecmd lignecmd) {
		getLignecmds().add(lignecmd);
		lignecmd.setProduit(this);

		return lignecmd;
	}

	public Lignecmd removeLignecmd(Lignecmd lignecmd) {
		getLignecmds().remove(lignecmd);
		lignecmd.setProduit(null);

		return lignecmd;
	}

	public List<Pack> getPacks() {
		return this.packs;
	}

	public void setPacks(List<Pack> packs) {
		this.packs = packs;
	}

	public Stock getStock() {
		return this.stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public List<ProduitOffre> getProduitOffres() {
		return this.produitOffres;
	}

	public void setProduitOffres(List<ProduitOffre> produitOffres) {
		this.produitOffres = produitOffres;
	}

	public ProduitOffre addProduitOffre(ProduitOffre produitOffre) {
		getProduitOffres().add(produitOffre);
		produitOffre.setProduit(this);

		return produitOffre;
	}

	public ProduitOffre removeProduitOffre(ProduitOffre produitOffre) {
		getProduitOffres().remove(produitOffre);
		produitOffre.setProduit(null);

		return produitOffre;
	}

}