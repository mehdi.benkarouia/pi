package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Date;
import java.util.List;


/**
 * The persistent class for the FactDevs database table.
 * 
 */
@Entity
@Table(name="FactDevs")
@NamedQuery(name="FactDev.findAll", query="SELECT f FROM FactDev f")
public class FactDev implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int factdevId;

	private Date date;

	private int etat;

	private double prix;

	private String reference;

	@Column(name="UserId")
	private int userId;

	//bi-directional many-to-one association to Lignecmd
	@OneToMany(mappedBy="factDev")
	private List<Lignecmd> lignecmds;

	public FactDev() {
	}

	public int getFactdevId() {
		return this.factdevId;
	}

	public void setFactdevId(int factdevId) {
		this.factdevId = factdevId;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getEtat() {
		return this.etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public double getPrix() {
		return this.prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getReference() {
		return this.reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public List<Lignecmd> getLignecmds() {
		return this.lignecmds;
	}

	public void setLignecmds(List<Lignecmd> lignecmds) {
		this.lignecmds = lignecmds;
	}

	public Lignecmd addLignecmd(Lignecmd lignecmd) {
		getLignecmds().add(lignecmd);
		lignecmd.setFactDev(this);

		return lignecmd;
	}

	public Lignecmd removeLignecmd(Lignecmd lignecmd) {
		getLignecmds().remove(lignecmd);
		lignecmd.setFactDev(null);

		return lignecmd;
	}

}