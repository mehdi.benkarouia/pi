package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;


/**
 * The persistent class for the ClientPhysique database table.
 * 
 */
@Entity
@NamedQuery(name="ClientPhysique.findAll", query="SELECT c FROM ClientPhysique c")
public class ClientPhysique implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String adresse;

	@Column(name="DateNaissance")
	private Date dateNaissance;

	private String mail;

	private String nom;

	private int numTel;

	private String password;

	private String prenom;

	private BigDecimal total;

	//bi-directional many-to-one association to Commande
	@OneToMany(mappedBy="clientPhysique")
	private List<Commande> commandes;

	//bi-directional many-to-one association to Commentaire
	@OneToMany(mappedBy="clientPhysique")
	private List<Commentaire> commentaires;

	//bi-directional many-to-one association to Lignecmd
	@OneToMany(mappedBy="clientPhysique")
	private List<Lignecmd> lignecmds;

	//bi-directional many-to-one association to Reclamation
	@OneToMany(mappedBy="clientPhysique")
	private List<Reclamation> reclamations;

	public ClientPhysique() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAdresse() {
		return this.adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public Date getDateNaissance() {
		return this.dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNumTel() {
		return this.numTel;
	}

	public void setNumTel(int numTel) {
		this.numTel = numTel;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public List<Commande> getCommandes() {
		return this.commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}

	public Commande addCommande(Commande commande) {
		getCommandes().add(commande);
		commande.setClientPhysique(this);

		return commande;
	}

	public Commande removeCommande(Commande commande) {
		getCommandes().remove(commande);
		commande.setClientPhysique(null);

		return commande;
	}

	public List<Commentaire> getCommentaires() {
		return this.commentaires;
	}

	public void setCommentaires(List<Commentaire> commentaires) {
		this.commentaires = commentaires;
	}

	public Commentaire addCommentaire(Commentaire commentaire) {
		getCommentaires().add(commentaire);
		commentaire.setClientPhysique(this);

		return commentaire;
	}

	public Commentaire removeCommentaire(Commentaire commentaire) {
		getCommentaires().remove(commentaire);
		commentaire.setClientPhysique(null);

		return commentaire;
	}

	public List<Lignecmd> getLignecmds() {
		return this.lignecmds;
	}

	public void setLignecmds(List<Lignecmd> lignecmds) {
		this.lignecmds = lignecmds;
	}

	public Lignecmd addLignecmd(Lignecmd lignecmd) {
		getLignecmds().add(lignecmd);
		lignecmd.setClientPhysique(this);

		return lignecmd;
	}

	public Lignecmd removeLignecmd(Lignecmd lignecmd) {
		getLignecmds().remove(lignecmd);
		lignecmd.setClientPhysique(null);

		return lignecmd;
	}

	public List<Reclamation> getReclamations() {
		return this.reclamations;
	}

	public void setReclamations(List<Reclamation> reclamations) {
		this.reclamations = reclamations;
	}

	public Reclamation addReclamation(Reclamation reclamation) {
		getReclamations().add(reclamation);
		reclamation.setClientPhysique(this);

		return reclamation;
	}

	public Reclamation removeReclamation(Reclamation reclamation) {
		getReclamations().remove(reclamation);
		reclamation.setClientPhysique(null);

		return reclamation;
	}

}