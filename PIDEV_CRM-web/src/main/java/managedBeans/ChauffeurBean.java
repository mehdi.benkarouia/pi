package managedBeans;

 

import java.io.File;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.Part;

import model.Chauffeur;
import services.ChaffeurService;

@ManagedBean(name="chauffeurBean")
@ApplicationScoped
public class ChauffeurBean {

	private String adresse;

	private String nom;

	private String prenom;

	private int tel;
    private Part img ;
	private String ville;
	
	private List<Chauffeur> chauffeurs;
	
	private int id;
	
	private Integer ChauffeurIdToBeUpadated;
	@EJB
	ChaffeurService chauffeurServ;

	
	public void addChauffeur() {
		try{
			   
	           InputStream in=img.getInputStream();

	           File f=new File("C:/Users/lenovo/Desktop/Outils/projets/PIDEV_CRM/PIDEV_CRM-web/src/main/webapp/assets/"+img.getSubmittedFileName());
	           f.createNewFile();
	           FileOutputStream out=new FileOutputStream(f);
	           
	           byte[] buffer=new byte[1024];
	           int length;
	           
	           while((length=in.read(buffer))>0){
	               out.write(buffer, 0, length);
	           }
	           
	           out.close();
	           in.close();
	           
	           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("C:/Users/lenovo/Desktop/Outils/projets/PIDEV_CRM/PIDEV_CRM-web/src/main/webapp/assets/", f.getAbsolutePath());
	           chauffeurServ.addChauffeur(new Chauffeur(adresse, img.getSubmittedFileName(), nom, prenom, tel, ville) );
	           Mailer m = new Mailer();
	   		   m.send("mehdi.benkarouia@esprit.tn", "Entreprise Activer !!", "Acceptation");
	        
	}catch(Exception e){
	           e.printStackTrace(System.out);
	       }
		
		
		
	}
	
	public List<Chauffeur> getChauffeurs() {
		return chauffeurs=chauffeurServ.getAllChaffeurs();
	}

	public void setChauffeurs(List<Chauffeur> chauffeurs) {
		this.chauffeurs = chauffeurs;
	}
    
    public void remove(Integer chauffeurId){
    	chauffeurServ.deleteChauffeurById(chauffeurId);
    }
   
    public void updateChauffeur(){
    	chauffeurServ.updateChauffeur(new Chauffeur(ChauffeurIdToBeUpadated,adresse,nom,prenom,tel,ville));
    }
    
    public void dipslayChauffeur(Chauffeur c) {
    	this.setAdresse(c.getAdresse());
    	this.setNom(c.getNom());
    	this.setPrenom(c.getPrenom());
    	this.setTel(c.getTel());
    	this.setVille(c.getVille());
    	this.setChauffeurIdToBeUpadated(c.getId());
    }
	public Integer getChauffeurIdToBeUpadated() {
		return ChauffeurIdToBeUpadated;
	}

	public void setChauffeurIdToBeUpadated(Integer chauffeurIdToBeUpadated) {
		ChauffeurIdToBeUpadated = chauffeurIdToBeUpadated;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getTel() {
		return tel;
	}

	public void setTel(int tel) {
		this.tel = tel;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public ChaffeurService getChauffeurServ() {
		return chauffeurServ;
	}

	public void setChauffeurServ(ChaffeurService chauffeurServ) {
		this.chauffeurServ = chauffeurServ;
	}
	
	public static long getSerialversionuid() {
		return getSerialversionuid(); //Sérialisation 
	}

	public Part getImg() {
		return img;
	}

	public void setImg(Part img) {
		this.img = img;
	}
	
}
